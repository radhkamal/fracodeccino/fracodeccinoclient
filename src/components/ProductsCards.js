import PropTypes from 'prop-types';
import { Card, Row, Col, CloseButton } from "react-bootstrap";
import { Link } from "react-router-dom";

const ProductsCards = ({ productProp }) => {
    const {_id, name, description, price} = productProp;
    return (
        <Card className="mb-2 text-center flexCard">
            <Card.Body>
                <Row>
                    <Col>
                        <img src="https://i.ibb.co/gyH4NCX/Frappe-2.png" alt="Placeholder image" className='placeholderImage'/>
                    </Col>
                    <Col>
                        <Card.Title className='crimsonText'>{name}</Card.Title>
                        <Card.Subtitle className='py-2'>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle className='py-2'>Price</Card.Subtitle>
                        <Card.Text>&#8369; {price}</Card.Text>
                        <Link className="btn btn-success productBtn py-2" to={`/product/${_id}`}>Details</Link>
                    </Col>
                </Row>
            </Card.Body>
        </Card>
    )
}

export default ProductsCards;
