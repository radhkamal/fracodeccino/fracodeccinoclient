import { useState, useEffect } from "react";
import { Carousel, Button, ListGroupItem } from "react-bootstrap";
import { Link } from "react-router-dom"; 


const AppCarouselPage = () => {
    const carouselItems = [
        {
            id: 1,
            img: {
                src: "https://i.ibb.co/H7J8zr2/1.png",
                alt: "First Slide"
            },
            captions: {
                title: "",
                text: "All sweets at Php119 only!"
            }
        },
        {
            id: 2,
            img: {
                src: "https://i.ibb.co/XYKFc1c/2.png",
                alt: "Second Slide"
            },
            captions: {
                title: "",
                text: ""
            }
        },
        {
            id: 3,
            img: {
                src: "https://i.ibb.co/RyhgmYs/4.png",
                alt: "Third Slide"
            },
            captions: {
                title: "",
                text: ""
            }
        }
    ];

    return (
        <Carousel>
            {
                carouselItems.map(item => {
                    const {id, img, captions} = item;
                    return(
                        <Carousel.Item className="frontPageItem" key={id}>
                            <img 
                                className="d-block w-100"
                                src={img.src} 
                                alt={img.alt}
                            />
                            <Carousel.Caption>
                                <h1 className="mb-5">{captions.title}</h1>
                                <h4 className="mb-5 promo pt-5">{captions.text}</h4>
                                <Button className="ctaBtn mb-5" as={Link} to="/shop">SIP NOW</Button>
                            </Carousel.Caption>
                        </Carousel.Item>
                    )
                })
            }
        </Carousel>
    )
}

export default AppCarouselPage;
